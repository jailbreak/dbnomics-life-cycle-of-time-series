---
# Cf https://hackmd.io/c/tutorials/%2Fs%2Fhow-to-create-slide-deck
title: DBnomics – life-cycle of a time series
revealOptions:
    transition: 'fade'
---

<style>
.reveal section img {
  border: none;
  box-shadow: none;
  background: none;
}
</style>

![](https://db.nomics.world/dbnomics-logo.svg)

<div style="margin-top: 3em"></div>

## Life-cycle of a time series
### from production to consumption

---

# Producing DBnomics data

---

![](https://i.imgur.com/0otD0Im.png)

---

## Provider

- several formats
  - XLS(X), CSV, XML (SDMX), JSON (JSON-Stat), HTML (tables, web pages), PDF
  - more or less machine-readable
- several protocols
  - bulk download VS stream
  - HTTP API, FTP, ZIP
- several limitations
  - bandwidth, query size, rate limiting, servers down or in error

---

![](https://i.imgur.com/xt5qqsT.png)

---

## Download script

- handles specificities of provider infrastructure
- produces files and directories
  - no DBnomics infrastructure to call

---

## Source data

- original data in the format of the provider
- store revisions
  - keeps provider history
  - most providers don't give access to
- using Git repositories

---

![](https://i.imgur.com/3BGkNhS.png)

---

## Convert script

- handles specificities of provider source data
- produces files and directories
  - no DBnomics infrastructure to call
- some code is shared between fetchers
  - common formats reading: SDMX (almost), JSON-Stat
  - data validation
- main problem: breaks when source data changes

---

## DBnomics data

must follow structured data model:
- provider → dataset → time series
- dataset dimensions
- observations attributes
- category tree kept from provider
- specified by JSON schemas and additional rules

---

## DBnomics data

- is stored in a common *storage* format
  - only directories and files
  - JSON for meta-data
  - TSV or JSON-Lines for data
- data model comes with a validation script
  - validates both abstract data model and concrete storage format

---

## Examples

### `provider.json`

```json
{
  "code": "AMECO",
  "name": "Annual macro-economic database of the European Commission's Directorate General for Economic and Financial Affairs",
  "region": "EU",
  "terms_of_use": "https://ec.europa.eu/info/legal-notice_en#copyright-notice",
  "website": "https://ec.europa.eu/info/business-economy-euro/indicators-statistics/economic-databases/macro-economic-database-ameco_en"
}
```

---

## Examples

### `AAGE/dataset.json`

```json
{
  "code": "AAGE",
  "name": "Average share of imports and exports of goods in world trade excluding intra EU trade :- Foreign trade statistics (1960-1998 Former EU-15, 1999-2001 Former EU-27)",
  "dimensions_codes_order": [
    "freq",
    "unit",
    "geo"
  ],
  "dimensions_labels": {
    "freq": "Frequency",
    "geo": "Country",
    "unit": "Unit"
  },
  "dimensions_values_labels": {
    "freq": {
      "a": "Annually"
    },
    "geo": {
      "aut": "Austria",
      "bel": "Belgium",
      "bgr": "Bulgaria",
    }
  },
  "series": [
    {
      "code": "EU28.1.0.0.0.AAGE",
      "dimensions": {
        "freq": "a",
        "geo": "eu28",
        "unit": "average-share"
      }
    },
    {
      "code": "EU27.1.0.0.0.AAGE",
      "dimensions": {
        "freq": "a",
        "geo": "eu27",
        "unit": "average-share"
      }
    },
  ]
}
```

---

## Examples

### `AAGE/AUT.1.0.0.0.AAGE.tsv`

```
PERIOD	VALUE
1960	NA
1961	NA
1962	NA
1963	0.5171329
1964	0.4985890
1965	0.5211119
1966	0.5107819
1967	0.5076950
1968	0.4978948
1969	0.5156083
1970	0.5453846
1971	0.5719287
1972	0.5817951
1973	0.5658592
1974	0.5500048
```

---

![](https://i.imgur.com/FNOHlRG.png)

---

## Fetchers

- one fetcher per provider
  - groups download and convert scripts
  - has its own Git repo
- isolated: reads files, writes files
- all are written in Python

---

## Fetchers

- download is launched by GitLab CI
  - scheduled 1/day/provider by default
  - some providers announce a calendar (machine-readable?)
- convert is launched by GitLab CI
  - after download, only if new data

---

![](https://i.imgur.com/pOrZkEP.png)

---

## Validate

- out of the scope of fetchers
- launched by GitLab CI
  - when DBnomics data changes
- applies JSON schemas to JSON files
- applies other rules
  - TSV files
  - directories structure
- helps detecting regressions in source data

---

![](https://i.imgur.com/nOFCReK.png)

---

## Index

- out of the scope of fetchers
- launched by GitLab CI
  - when DBnomics data changes
- using [Apache Solr](https://lucene.apache.org/solr/)
- powers
  - full-text search
  - faceted search (by dimension)
- only latest revisions
- incremental

---

## Contributing

contributors can submit new fetchers:
- "just" read / write files
- reviewed and enabled by DBnomics team
- will benefit from DBnomics infrastructure
- can be written in any other language
  - question: how to maintain them?

---

# Consuming DBnomics data

---

![](https://i.imgur.com/oIhvqlW.png)

---

## Bulk download

- no search
- to download a whole provider
- since DBnomics data is stored in Git
  - common source code management tool
- revisions are also downloaded

---

## Targeting specific series

- full-text search
- search by dimension
- browse interactively

---

## Web API

- HTTPS URLs returning JSON
- relies on Index
- documented by [OpenAPI](https://api.db.nomics.world/v22/apidocs)
- produces series in XLSX and CSV
- reserved to advanced technical users

---

## Clients

- call Web API to download series
- supported programming languages
  - R, Python, Julia, Stata
  - as `DataFrame`
- tools:
  - [Time series editor](https://editor.nomics.world/)
  - [Gretl](http://gretl.sourceforge.net/) (community)

---

## Web site

- presented data comes from Web API
- browse providers, datasets, series
- full-text search
- filter by dimension interactively
- shows time series as data tables or charts
- allows to add time series to cart
  - from different providers
- modern UX design
  - responsive, no page reload...

---

![](https://i.imgur.com/ia66btm.png)

---

![](https://i.imgur.com/cQICOtf.png)

---

![](https://i.imgur.com/yfkl2dA.png)

---

![](https://i.imgur.com/bDfJa9Z.png)

---

## Technologies

- DBnomics is mostly written in [Python](https://www.python.org/)
  - Web API uses [Pandas](https://pandas.pydata.org/)
- [Git](https://git-scm.com/) for data versioning
- [GitLab](https://about.gitlab.com/) with CI for running jobs
- [Apache Solr](https://lucene.apache.org/solr/) for indexation
- JavaScript for the web site
  - using [Sapper](https://sapper.svelte.dev/), [Svelte](https://svelte.dev/) and [Plotly](https://plot.ly/), [tailwindcss](https://tailwindcss.com)

---

## Concerns

- current limitations
  - big providers + Git
    - had to introduce JSON-Lines
    - cloning is long
    - past revisions needs RAM
  - data model can be simplified
- data model can evolve
  - how to keep backward compatibility?
    - storage format public or black-box?

---

## Evolutions

- infrastructure resilience / scalability
- share more functions between fetchers
- common formats outputs
  -  SDMX, JSON-Stat
- display revisions in web site

---

## Questions time
