# DBnomics – Life-cycle of a time series

## Install dependencies

Requires nodejs >= 10 and npm.

```
npm install
```

## Build static website

```
npm run export
```

then run `npx serve --symlinks _static`.

## Export PDF

```
npm run pdf
```

## Develop

```
npm run dev
```

Change `slides.md` or `theme/source/dbnomics.scss`.

## Theme

Theme is stored in `dbnomics.css` and comes from https://github.com/cbenz/reveal.js/blob/dbnomics-theme/css/theme/dbnomics.css
